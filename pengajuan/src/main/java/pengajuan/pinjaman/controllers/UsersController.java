package pengajuan.pinjaman.controllers;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import ch.qos.logback.core.Context;

import pengajuan.pinjaman.models.Users;
import pengajuan.pinjaman.repositories.UserRepo;

@Controller
@RequestMapping(value="/")
public class UsersController {
    @Autowired UserRepo userrepo;

    @GetMapping(value = "/")
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("index");
        return view;
         
    }
    @GetMapping(value = "/formlogin")
    public ModelAndView formlogin(){
        ModelAndView view = new ModelAndView("/login");
        return view;
         
    }
    @GetMapping(value = "/registrasi")
    public ModelAndView formregistrasi(){
        ModelAndView view = new ModelAndView("/registrasi");
        Users user = new Users();
        view.addObject("user", user);
        return view;
    }
    private static String bytesToHex(byte[] hash) {
	    StringBuilder hexString = new StringBuilder(2 * hash.length);
	    for (int i = 0; i < hash.length; i++) {
	        String hex = Integer.toHexString(0xff & hash[i]);
	        if(hex.length() == 1) {
	            hexString.append('0');
	        }
	        hexString.append(hex);
	    }
	    return hexString.toString();
	}

    private static String UPLOADED_FOLDER = "C:\\Users\\Windows\\Documents\\Bootcamp\\project\\pengajuan\\assets\\photo\\";
    @PostMapping(value="/regis")
	public ModelAndView regis(
			@ModelAttribute Users users, 
			BindingResult result,
			@RequestParam("photofile") MultipartFile file) throws Exception
	{
		if(!result.hasErrors()) {
			try {
				if(file.getOriginalFilename() != "") { 
					byte[] bytes = file.getBytes();
					Path path = Paths.get(UPLOADED_FOLDER+file.getOriginalFilename());
					Files.write(path, bytes);
				}
				
				String pass = (String) result.getFieldValue("password");
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				byte[] encodehash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));
				users.setPassword(bytesToHex(encodehash));
				this.userrepo.save(users);
				
				
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
		return new ModelAndView("redirect:/");
	}
    @PostMapping(value = "ceklogin")
    public ModelAndView cekLogin(@ModelAttribute Users users, BindingResult result, HttpSession sess){
        String redirect = "";
        if(!result.hasErrors()){
            String email = (String) result.getFieldValue("email");
            String password = (String) result.getFieldValue("password");
            try {
				MessageDigest digest;
				digest = MessageDigest.getInstance("SHA-256");
				byte[] encodehash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
				String pass = bytesToHex(encodehash);
				
				List<Users> getuser = this.userrepo.getLogin(email, pass);
				
				try {
					sess.setAttribute("uid", getuser.get(0).getId());
                    sess.setAttribute("email", getuser.get(0).getEmail());
					sess.setAttribute("username", getuser.get(0).getUsername());
					sess.setAttribute("fullname", getuser.get(0).getFullname());
					
					System.out.println("Access Granted!");
					redirect = "redirect:/home";
					
					if(getuser.get(0).getId()==null) {
						redirect = "redirect:/";
						
					}else {
						redirect = "redirect:/home";
						
					}
				} catch (Exception e) {
					System.out.println("Access Denied!");
					redirect = "redirect:/";
					
				}
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				redirect = "redirect:/";
			}
		}
		return new ModelAndView(redirect);
        }
        @GetMapping(value = "/home")
        public ModelAndView home(HttpSession sess){
            ModelAndView view = new ModelAndView("/transaksi/home");
			view.addObject("fullname", sess.getAttribute("fullname"));
            return view;
        }
    }


