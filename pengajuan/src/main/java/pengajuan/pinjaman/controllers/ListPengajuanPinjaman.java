package pengajuan.pinjaman.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import pengajuan.pinjaman.models.Pengajuan;
import pengajuan.pinjaman.models.Relation;
import pengajuan.pinjaman.repositories.PengajuanRepo;
import pengajuan.pinjaman.repositories.RelationRepo;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/pinjaman/")
public class ListPengajuanPinjaman {
    @Autowired RelationRepo relasirepo;
    @Autowired PengajuanRepo pengajuanrepo;
    
    @GetMapping(value = "/index")
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("/transaksi/listpinjaman");
        return view;
    }

    @GetMapping("listpinjaman")
    public ResponseEntity<List<Pengajuan>> listpinjaman(){
        try {
            List<Pengajuan> listpin = this.pengajuanrepo.findAll();
            List<Pengajuan> sortpin = new ArrayList<Pengajuan>();
            sortpin = (List<Pengajuan>) listpin.stream().sorted((p, q) -> p.getPeminjaman().compareTo(q.getPeminjaman())).collect(Collectors.toList());
            return new ResponseEntity<>(sortpin, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
        	e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("pinjamancus")
    public ResponseEntity<List<Relation>> PinjamanCus(){
    	try {
    		List<Relation> listpeminjamanById = this.relasirepo.peminjamancus((long)1);
        	return new ResponseEntity<>(listpeminjamanById, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
    }
    
}
