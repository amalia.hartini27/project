package pengajuan.pinjaman.controllers;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import pengajuan.pinjaman.models.Pengajuan;
import pengajuan.pinjaman.models.Relation;
import pengajuan.pinjaman.models.Users;
import pengajuan.pinjaman.repositories.PengajuanRepo;
import pengajuan.pinjaman.repositories.RelationRepo;
import pengajuan.pinjaman.repositories.UserRepo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@CrossOrigin("*")
@RequestMapping(value = "/transaksi/")
public class PengajuanController {
    @Autowired PengajuanRepo pengajuanrepo;
    @Autowired RelationRepo relationrepo;

    @GetMapping(value="/index") 
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("/transaksi/home");
        return view;
    }
    
    @GetMapping(value = "/pengajuan/")
    public ModelAndView Pengajuan(){
        ModelAndView view = new ModelAndView("/transaksi/pengajuan");
        return view;
    }


    @PostMapping("/pengajuan/")
    public ResponseEntity<Pengajuan> pengajuanPin(@RequestBody Pengajuan pengajuan){
        try {
            Pengajuan pengajuanpinjaman = this.pengajuanrepo.save(pengajuan);
            if(pengajuanpinjaman.equals(pengajuan)){
                return new ResponseEntity<Pengajuan>(pengajuan, HttpStatus.OK);
            }else{
                return new ResponseEntity<Pengajuan>(pengajuan, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            //TODO: handle exception
            e.printStackTrace();
            return new ResponseEntity<Pengajuan>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("/pengajuan/get/{hid}")
	public ResponseEntity<List<Pengajuan>> getPinById(@PathVariable("hid") Long hid){
		try {
			List<Pengajuan> pengajuan = this.pengajuanrepo.pengajuanbyid(hid);
			return new ResponseEntity<List<Pengajuan>>(pengajuan, HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List<Pengajuan>>( HttpStatus.NO_CONTENT);
		}
	}
    @PostMapping("/pinjaman")
    public ResponseEntity<Relation> PengajuanPinjaman(HttpSession sess, @RequestBody Relation relation){
    	try {
    		
    		Relation relationpengajuan  = this.relationrepo.save(relation);
    		if(relationpengajuan.equals(relation)){
                return new ResponseEntity<Relation>(relation, HttpStatus.OK);
            }else{
                return new ResponseEntity<Relation>(relation, HttpStatus.BAD_REQUEST);
            }
    	}catch (Exception e) {
            //TODO: handle exception
            e.printStackTrace();
            return new ResponseEntity<Relation>(HttpStatus.NO_CONTENT);
        }
    }
   
   
}
