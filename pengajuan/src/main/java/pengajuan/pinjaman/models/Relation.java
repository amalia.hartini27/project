package pengajuan.pinjaman.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="relation")
public class Relation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="id_user")
    private Long id_user;
    @ManyToOne //hubungan antar tabel
	@JoinColumn(name="id_user", insertable = false, updatable = false)//join dan menandakan bahwa tidak bisa diisi sembarangan
	public Users users; //harus public


    @Column(name="id_pengajuan")
    private Long id_pengajuan;
    @ManyToOne //hubungan antar tabel
	@JoinColumn(name="id_pengajuan", insertable = false, updatable = false)//join dan menandakan bahwa tidak bisa diisi sembarangan
	public Pengajuan pengajuan; //harus public

    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public Long getId_pengajuan() {
        return id_pengajuan;
    }

    public void setId_pengajuan(Long id_pengajuan) {
        this.id_pengajuan = id_pengajuan;
    }

   
    
    
}
