package pengajuan.pinjaman.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pengajuan.pinjaman.models.Pengajuan;

public interface PengajuanRepo extends JpaRepository<Pengajuan, Long>{
    @Query(value="FROM Pengajuan WHERE id = ?1")
	List<Pengajuan> pengajuanbyid(Long id);
}
