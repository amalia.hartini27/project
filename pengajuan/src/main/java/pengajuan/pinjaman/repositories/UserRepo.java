package pengajuan.pinjaman.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pengajuan.pinjaman.models.Users;

@Repository
public interface UserRepo extends JpaRepository<Users, Long>{
    @Query(value="SELECT*FROM users u WHERE u.email=?1 AND u.password=?2",
    nativeQuery = true)
    List<Users> getLogin(String email, String password);    
}
