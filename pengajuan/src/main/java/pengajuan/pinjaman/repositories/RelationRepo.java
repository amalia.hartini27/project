package pengajuan.pinjaman.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pengajuan.pinjaman.models.Relation;

public interface RelationRepo extends JpaRepository<Relation, Long>{
    @Query(value = "SELECT*FROM relation WHERE id_user=?1", nativeQuery = true)
    List<Relation> peminjamancus(Long id);
}
